import { Component, OnInit } from '@angular/core';
import { routerTransition } from '../../router.animations';
import { ServiceService } from '../../commonservice/service.service';

@Component({
    selector: 'app-dashboard',
    templateUrl: './dashboard.component.html',
    styleUrls: ['./dashboard.component.scss'],
    animations: [routerTransition()]
})
export class DashboardComponent implements OnInit {
    public alerts: Array<any> = [];
    public sliders: Array<any> = [];

    // Pie1
    public pieChartLabels1: string[] = [
        'Open',
        'Closed',
        'Re-Open',
        'Running'
    ];
    pieChartData1: number[] ;
    public pieChartType1: string;

    // Pie2
    public pieChartLabels: string[] = [
        'Main Category',
        'Sub Category',
        'Vendor',
        'User'
    ];
    public pieChartData: number[] = [3, 4, 3, 1];
    public pieChartType: string;

    // events
    public chartClicked(e: any): void {
        // console.log(e);
    }

    public chartHovered(e: any): void {
        // console.log(e);
    }

    constructor( private authService: ServiceService) {
        this.pieChartData1 = [4, 2, 1, 1];
        this.sliders.push(
            {
                imagePath: 'assets/images/slider1.jpg',
                label: 'First slide label',
                text:
                    'First slide details.'
            },
            {
                imagePath: 'assets/images/slider2.jpg',
                label: 'Second slide label',
                text: 'Second slide details.'
            },
            {
                imagePath: 'assets/images/slider3.jpg',
                label: 'Third slide label',
                text:
                    'Third slide details.'
            }
        );

        // this.alerts.push(
        //     {
        //         id: 1,
        //         type: 'success',
        //         message: `Lorem ipsum dolor sit amet, consectetur adipisicing elit.
        //         Voluptates est animi quibusdam praesentium quam, et perspiciatis,
        //         consectetur velit culpa molestias dignissimos
        //         voluptatum veritatis quod aliquam! Rerum placeat necessitatibus, vitae dolorum`
        //     },
        //     {
        //         id: 2,
        //         type: 'warning',
        //         message: `Lorem ipsum dolor sit amet, consectetur adipisicing elit.
        //         Voluptates est animi quibusdam praesentium quam, et perspiciatis,
        //         consectetur velit culpa molestias dignissimos
        //         voluptatum veritatis quod aliquam! Rerum placeat necessitatibus, vitae dolorum`
        //     }
        // );
    }

    ngOnInit() {
        this.pieChartType1 = 'pie';
        this.pieChartType = 'pie';
    }

    public closeAlert(alert: any) {
        const index: number = this.alerts.indexOf(alert);
        this.alerts.splice(index, 1);
    }

    GetStatus() {
       // this.spinnerService.show()
        this.authService.getAllStatus().then(
          (result: any) => {
              console.log("Api Respone Count",result)
              if(result['status']) {
               // this.allCountStatus = result
                // this.LeadStatusClosed =  result.LeadStatusClosed ;
                // this.LeadStatusOpen = result.LeadStatusOpen ;
                // this.LeadStatusReOpen = result.LeadStatusReOpen;
                // this.LeadStatusRunning = result.LeadStatusRunning;
                // this.LeadTotal= result.LeadTotal;

                // this.MainCatCount = result.MainCatCount;
                // this.SubCatCount= result.SubCatCount;
                // this.UserRegistration = result.UserRegistration;
                // this.VendorRegistration = result.VendorRegistration;


              //  this.spinnerService.hide();
              }




          }, (err) => {

            if (err.status === 405 || err.status === 500 || err.status === 0) {
            }

          }
        );
      }
}
